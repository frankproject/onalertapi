<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Regulation extends Model
{
    protected $table = 'regulations';
    protected $fillable = ['case_id', 'type', 'owner', 'folio', 'request_id'];
    protected $dates = ['deleted_at'];

    public function request() {
        return $this->belongsTo('App\Request', 'request_id');
    }

    public function regulation_details() {
        return $this->hasMany('App\RegulationDetail', 'regulation_id');
    }

}
