<?php 

namespace App;
  
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
  
class Folio extends Model
{
     protected $table = 'folios';
     protected $fillable = ['name'];
     protected $dates = ['deleted_at'];
     
}