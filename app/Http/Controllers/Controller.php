<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Goutte\Client;
use GuzzleHttp\Client as GuzzleClient;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $goutteClient;

    public function listRegulationsForm ($folio) {

        $url_form1 = config('sites.miami_dade_regulation_form1.url');

        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $this->goutteClient->setClient($guzzleClient);

        $params = explode('-', $folio);
        
        $queryParams = [
            'FolioA='.$params[0],
            'FolioB='.$params[1],
            'FolioC='.$params[2],
            'FolioD='.$params[3],
            'CaseType=All Cases'
        ];

        $content = implode('&', $queryParams);

        $crawler = $this->goutteClient->request('POST', 
                                    $url_form1, 
                                    [], 
                                    [], 
                                    ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'], 
                                    $content);

        
        //var_dump($crawler->html()); 

        $GLOBALS['regulation'] = array (
            'id' => '',
            'type' => '',
            'address' => '',
            'owner' => '',
            'folio' => ''
        ); 

        $GLOBALS['count'] = 0;
        $GLOBALS['regulations'] = array ();
        $crawler->filter('form > tr > td')->each(function ($node) {
            
            if ( $GLOBALS['count'] == 0 ) {
                $GLOBALS['regulation']['id'] = $node->text();
            } else if ( $GLOBALS['count'] == 1 ) {
                $GLOBALS['regulation']['type'] = $node->text(); 
            } else if ( $GLOBALS['count'] == 2 ) {
                $GLOBALS['regulation']['address'] = $node->text();  
            } else if ( $GLOBALS['count'] == 3 ) {
                $GLOBALS['regulation']['owner'] = $node->text(); 
            } else if ( $GLOBALS['count'] == 4 ) {
                $GLOBALS['regulation']['folio'] = $node->text(); 
            }     

            $GLOBALS['count']++;

            if ( $GLOBALS['count'] == 5 ) {
                
                array_push($GLOBALS['regulations'], $GLOBALS['regulation']);
                $GLOBALS['count'] = 0;
            }      

        });    

        return $GLOBALS['regulations'];

        
    }

    public function getRegulationsForm ($id) {

        $url_form2 = config('sites.miami_dade_regulation_form2.url');

        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $this->goutteClient->setClient($guzzleClient);
        
        $queryParams = [
            'casenum='.$id
        ];

        $content = implode('&', $queryParams);

        $crawler = $this->goutteClient->request('POST', 
                                    $url_form2, 
                                    [], 
                                    [], 
                                    ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'], 
                                    $content);

        
        //Getting first table
        $first_table = $crawler->filter('table table')->first();


         $GLOBALS['info'] = array (
            'id' => '',
            'address' => '',
            'description' => '',
            'case_opened' => '',
            'case_closed' => '',
            'deputy_clerk' => '',
            'inspector' => '',
            'permit_number' => '',
            'interested_parties' => '',
            'owner_info' => '',
            'building_code' => '',
            'comments' => ''
        ); 

        //Getting all values with keys
        $first_table->filter('tr')->each(function ($node) {
            
            $key = $node->filter('td')->first()->text();
            $value = $node->filter('td')->last()->text();

            if ( $key == "Case Number" ) {
                $GLOBALS['info']['id'] = $this->clean_tags($value);
            }
            else if ( $key == "Address" ) {
                $GLOBALS['info']['address'] = $this->clean_tags($value);
            }
            else if ( $key == "Folio Number" ) {
                $GLOBALS['info']['id'] = $this->clean_tags($value);
            }
            else if ( $key == "Legal Description" ) {
                $GLOBALS['info']['description'] = $this->clean_tags($value);
            }
            else if ( $key == "Case Opened On" ) {
                $GLOBALS['info']['case_opened'] = $this->clean_tags($value);
            }
            else if ( $key == "Case Closed On" ) {
                $GLOBALS['info']['case_closed'] = $this->clean_tags($value);
            }
            else if ( $key == "Deputy Clerk" ) {
                $GLOBALS['info']['deputy_clerk'] = $this->clean_tags($value);
            }
            else if ( $key == "Inspector" ) {
                $GLOBALS['info']['inspector'] = $this->clean_tags($value);
            }
            else if ( $key == "Permit Number" ) {
                $GLOBALS['info']['permit_number'] = $this->clean_tags($value);
            }
            else if ( $key == "Interested Parties" ) {
                $GLOBALS['info']['interested_parties'] = $this->clean_tags($value);
            }
            else if ( $key == "Owner Information" ) {
                $GLOBALS['info']['owner_info'] = $this->clean_tags($value);
            }
            else if ( $key == "Building Code" ) {
                $GLOBALS['info']['building_code'] = $this->clean_tags($value);
            }
            else if ( $key == "Additional Comments" ) {
                $GLOBALS['info']['comments'] = $this->clean_tags($value);
            }

        });  

        return $GLOBALS['info'];

        
    }

    public function getRegulationsFormTables ($id) {

        $url_form2 = config('sites.miami_dade_regulation_form2.url');

        $this->goutteClient = new Client();
        $guzzleClient = new GuzzleClient(array(
            'timeout' => 60,
        ));
        $this->goutteClient->setClient($guzzleClient);
        
        $queryParams = [
            'casenum='.$id
        ];

        $content = implode('&', $queryParams);

        $crawler = $this->goutteClient->request('POST', 
                                    $url_form2, 
                                    [], 
                                    [], 
                                    ['HTTP_CONTENT_TYPE' => 'application/x-www-form-urlencoded'], 
                                    $content);

        
      
        $GLOBALS['tables'] = array ();
        $crawler->filter('table table')->each(function ($node) {
            array_push( $GLOBALS['tables'], '<table>' . $node->html() . '</table>' );
        });

        return $GLOBALS['tables'];

        
    }

    private function clean_tags ($string ) {

        $string = str_replace ("\t", "", $string);
        $string = str_replace ("\n", " ", $string);

        return $string;

    }
}
