<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignAttrTableRegulations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('regulations', 'request_id')) {

            Schema::table('regulations', function (Blueprint $table) {
                $table->integer('request_id')->unsigned();
                $table->foreign('request_id')->references('id')->on('requests');

            });

        };
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
