<?php
  
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\OnAlertRequest;
use App\Regulation;
use App\RegulationDetail;
 
class OnAlertRequestsController extends Controller
{
    protected $respose;
 
    public function __construct(Response $response)
    {
        $this->response = $response;
    }
 
    public function index()
    {
        try {
            $Requests = OnAlertRequest::orderBy('id', 'desc')->paginate(25);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'requests' => $Requests->toArray(),
        ), 200);
    }
 
    public function show($id)
    {
        try {
            $Request = OnAlertRequest::find($id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'request' => $Request
        ), 200);
    }
 
    public function destroy($id)
    {
        try {
            $Request = OnAlertRequest::find($id);
            $Request->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Request deleted successfully',
            'request' => $Request
        ), 200);
 
    }

    public function create(Request $request)
    {
        try {
            $Request = OnAlertRequest::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Request created successfully',
            'request' => $Request
        ), 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $Request = OnAlertRequest::find($id);
            $Request->update($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Request updated successfully',
            'request' => $Request
        ), 200);
    }

    public function listRegulations(Request $request)
    { /*30-4926-015-0270*/
        //For list Regulation
        try {
            //var_dump ($request->all());die;
            $Regulations = $this->listRegulationsForm($request->folio);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        //Save Request
        try {
            $Request = OnAlertRequest::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        //Save Regulations
        try {
            foreach ($Regulations as $Regulation) {
                $regulation_data = array (
                    'case_id' => $Regulation['id'],
                    'type' => $Regulation['type'],
                    'owner' => $Regulation['owner'],
                    'folio' => $Regulation['folio'],
                    'address' => $Regulation['address'],
                    'request_id' => $Request['id']
                );
                
                $regulation_new = Regulation::create($regulation_data);
                
                $regulations_detail = $this->getRegulationsForm($Regulation['id']);

                $regulation_detail_data = array (
                    'case_id' => $regulations_detail['id'],
                    'address' => $regulations_detail['address'],
                    'description' => $regulations_detail['description'],
                    'case_opened' => $regulations_detail['case_opened'],
                    'case_closed' => $regulations_detail['case_closed'],
                    'deputy_clerk' => $regulations_detail['deputy_clerk'],
                    'inspector' => $regulations_detail['inspector'],
                    'permit_number' => $regulations_detail['permit_number'],
                    'interested_parties' => $regulations_detail['interested_parties'],
                    'owner_info' => $regulations_detail['owner_info'],
                    'building_code' => $regulations_detail['building_code'],
                    'comments' => $regulations_detail['comments'],
                    'regulation_id' => $regulation_new->id
                );

                RegulationDetail::create($regulation_detail_data);



            }
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }


        return response(array(
            'error' => false,
            'requests' => $Regulations,
        ), 200);
    }

    public function getRegulation(Request $request)
    { 
        try {
            $Requests = $this->getRegulationsForm($request->id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'requests' => $Requests,
        ), 200);
    }

    public function getRegulationsTables(Request $request)
    { 
        try {
            $Requests = $this->getRegulationsFormTables($request->id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'requests' => $Requests,
        ), 200);
    }
 
}
