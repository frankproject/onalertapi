<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegulationDetail extends Model
{
    protected $table = 'regulation_details';
    protected $fillable = [
        'case_id', 
        'address', 
        'description', 
        'case_opened', 
        'case_closed', 
        'deputy_clerk',
        'inspector',
        'permit_number',
        'interested_parties',
        'owner_info',
        'building_code',
        'comments',
        'regulation_id'
        ];
    protected $dates = ['deleted_at'];

    public function regulation() {
        return $this->belongsTo('App\Regulation', 'regulation_id');
    }
}
