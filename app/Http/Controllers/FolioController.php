<?php
  
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use App\Http\Requests;
use EllipseSynergie\ApiResponse\Contracts\Response;
use App\Folio;
 
class FolioController extends Controller
{
    protected $respose;
 
    public function __construct(Response $response)
    {
        $this->response = $response;
    }
 
    public function index()
    {
        try {
            $folios = Folio::orderBy('id', 'desc')->paginate(25);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'folios' => $folios->toArray(),
        ), 200);
    }
 
    public function show($id)
    {
        try {
            $Folio = Folio::find($id);
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'folio' => $Folio
        ), 200);
    }
 
    public function destroy($id)
    {
        try {
            $Folio = Folio::find($id);
            $Folio->delete();
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Folio deleted successfully',
            'folio' => $Folio
        ), 200);
 
    }

    public function create(Request $request)
    {
        try {
            $Folio = Folio::create($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Folio created successfully',
            'folio' => $Folio
        ), 200);
    }

    public function update(Request $request, $id)
    {
        try {
            $Folio = Folio::find($id);
            $Folio->update($request->all());
        } catch (\Exception $ex) {
            return response(array(
                'error' => $ex
            ));
        }

        return response(array(
            'error' => false,
            'message' => 'Folio updated successfully',
            'folio' => $Folio
        ), 200);
    }
 
}