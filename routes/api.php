<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




// get list of folios
Route::get('folios','FolioController@index');
// get specific folio
Route::get('folio/{id}','FolioController@show');
// delete a folio
Route::delete('folio/{id}','FolioController@destroy');
// update existing folio
Route::put('folio','FolioController@update');
// create new folio
Route::post('folio','FolioController@create');


// get list of requests
Route::get('requests','OnAlertRequestsController@index');
// get specific request
Route::get('request/{id}','OnAlertRequestsController@show');
// delete a request
Route::delete('request/{id}','OnAlertRequestsController@destroy');
// update existing request
Route::put('request','OnAlertRequestsController@update');
// create new request
Route::post('request','OnAlertRequestsController@create');
// list Regulations
Route::get('regulations','OnAlertRequestsController@listRegulations');
// get Regulation Info
Route::get('regulation','OnAlertRequestsController@getRegulation');
// get Regulations Tables
Route::get('regulations-table','OnAlertRequestsController@getRegulationsTables');