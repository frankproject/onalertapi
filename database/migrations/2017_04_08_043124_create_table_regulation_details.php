<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRegulationDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regulation_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('case_id');
            $table->string('address')->nullable();
            $table->string('description')->nullable();
            $table->string('case_opened')->nullable();
            $table->string('case_closed')->nullable();
            $table->string('deputy_clerk')->nullable();
            $table->string('inspector')->nullable();
            $table->string('permit_number')->nullable();
            $table->string('interested_parties')->nullable();
            $table->string('owner_info')->nullable();
            $table->string('building_code')->nullable();
            $table->string('comments')->nullable();
            $table->timestamps();

            $table->softDeletes();

            $table->integer('regulation_id')->unsigned();
            $table->foreign('regulation_id')->references('id')->on('regulations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('regulation_details');
    }
}
