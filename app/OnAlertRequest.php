<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OnAlertRequest extends Model
{
    protected $table = 'requests';
    protected $fillable = ['address', 'folio', 'type'];
    protected $dates = ['deleted_at'];

    public function regulations() {
        return $this->hasMany('App\Regulation', 'request_id');
    }
}
